package com.corp.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest
{
   protected WebDriver driver = null;
   String driverPath = "../CreditRepairCloud/src/main/java/drivers/chromedriver.exe";
    
   public WebDriver getDriver(WebDriverTypeEnum value)
   {
	   switch(value)
	   { 
	   case CHROME:
		   System.setProperty("webdriver.chrome.driver",driverPath);
		   driver = new ChromeDriver();
		   break;
		   
	   case IE:
		   
		   //IE code comes here
		   
	   case SAFARI:
		   
		   //Safari code comes here
		   
	   case OPERA:
		   
		   //Opera code comes here
		   
		   default:
			   System.setProperty("webdriver.chrome.driver",driverPath);
			   driver = new ChromeDriver();
	   }
	return driver;
	   
	   
	   
   }
   
   // this method will wait for element 
   
  public void waitForElement(WebDriver driver,By element,long timeInSeconds)
  {
	  try {
		  WebDriverWait wait = new WebDriverWait(driver, timeInSeconds);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		  
	  }catch(ElementNotVisibleException e)
	  {
		  e.printStackTrace();
	  }
	  
	  
  }
}
