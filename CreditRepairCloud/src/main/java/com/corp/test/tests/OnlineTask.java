package com.corp.test.tests;

import static org.testng.Assert.assertTrue;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.corp.test.pages.AddNewClientPage;
import com.corp.test.pages.ClientTabPage;
import com.corp.test.pages.HomePage;
import com.corp.test.pages.LogonPage;
import com.corp.test.utils.BaseTest;
import com.corp.test.utils.WebDriverTypeEnum;


public class OnlineTask extends BaseTest
{
	WebDriver driver =null;
	String url;
	String userName;
	String password;
	String firstName;
	String lastName;
	String email;
	String phoneNumber;
	String ssn;
	String address;
	String city;
	String zipcode;
	String state;
	
	
	@BeforeTest
	
	public void setUp() throws IOException
	{
		FileReader reader = new FileReader("../CreditRepairCloud/src/main/java/data/"+this.getClass().getSimpleName()+".properties");
		Properties data= new Properties();
		data.load(reader);
		
		url = data.getProperty("Url");
		userName = data.getProperty("UserName");
		password = data.getProperty("Password");
		firstName = data.getProperty("FirstName");
		lastName = data.getProperty("LastName");
		email = data.getProperty("Email");
		phoneNumber = data.getProperty("PhoneNumber");
		ssn = data.getProperty("SSN");
		address = data.getProperty("Address");
		city = data.getProperty("City");
		zipcode = data.getProperty("ZipCode");
		state = data.getProperty("State");
		   
		driver = getDriver(WebDriverTypeEnum.CHROME);
		driver.manage().deleteAllCookies();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  
		   
		   
	}
	
	@Test
	
	public void OperationsOnCreditRepair()
	{
		
		  
		try
		{
			LogonPage login = new LogonPage(driver);
			
			assertTrue(login.enterUsername(userName), "Failed to Enter User Name");
			
			assertTrue(login.enterPassword(password), "Failed to Enter Password");
			
			assertTrue(login.clickLogOn(), "Failed to Click Log On Button");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		  
		try
		{
			HomePage home = new HomePage(driver);
			
			assertTrue(home.isPageLoaded(), "Failed to Validate whether Home page is loaded or not");
			
			assertTrue(home.clickOnMyClientsTab(), "Failed to Click My Clients Tab");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		  
		
		try
		{
			ClientTabPage  client = new ClientTabPage(driver);
			
		
			assertTrue(client.clickOnAddNewClient(), "Failed to Click on Add New Client Button");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		  
		try
		{
			AddNewClientPage newClient = new AddNewClientPage(driver);
			
			assertTrue(newClient.enterFirstName(firstName), "Failed to Enter First Name");
			
			assertTrue(newClient.enterLastName(lastName), "Failed to Enter Last Name");
			
			assertTrue(newClient.enterEmail(email), "Failed to Enter Email");
			
			assertTrue(newClient.enterLastFourDigitsOfSSN(ssn), "Failed to Enter Last 4 Digits of SSN");
			
			assertTrue(newClient.enterPhoneNumber(phoneNumber), "Failed to Enter Phone Number");
			
			assertTrue(newClient.enterMailingAddress(address), "Failed to Enter Address");
			
			assertTrue(newClient.enterCity(city), "Failed to Enter City Name");
			
			assertTrue(newClient.enterZipCode(zipcode), "Failed to Enter Zip Code");
			
			assertTrue(newClient.selectState(state), "Failed to Select State");
			
			assertTrue(newClient.clickSubmit(), "Failed to Click Submit Button");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		  
		  
		  
		   
	}
	
	@AfterMethod
	
	public void teardown()
	{
		if(null!=driver)
		{
			driver.close();
			driver.quit();
		}
	}

}
