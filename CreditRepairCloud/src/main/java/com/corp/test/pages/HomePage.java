package com.corp.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.corp.test.utils.BaseTest;

public class HomePage extends BaseTest
{
	
	private static final By IS_HOMEPAGE_LOADED = By.xpath("//div[@class='logo']");
	private static final By MY_CLIENTS_TAB   = By.xpath("//a[contains(text(),'My Clients')]");
		
		public HomePage(WebDriver driver)
		{
			this.driver = driver;
		}
		
		//Is Page Loaded
		
		public boolean isPageLoaded()
		{
			try
			{
				waitForElement(driver, IS_HOMEPAGE_LOADED, 30);
				driver.findElement(IS_HOMEPAGE_LOADED);
				
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
			}
			
			return true;
			
		}
		
		
				
				//Click on My Clients Tab
				
				public boolean clickOnMyClientsTab()
				{
					try
					{
						waitForElement(driver, MY_CLIENTS_TAB, 30);
						driver.findElement(MY_CLIENTS_TAB).click();
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}

}
