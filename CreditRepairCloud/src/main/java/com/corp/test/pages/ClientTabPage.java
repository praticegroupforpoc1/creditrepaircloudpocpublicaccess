package com.corp.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.corp.test.utils.BaseTest;

public class ClientTabPage extends BaseTest
{
	
	
	private static final By ADD_NEW_CLIENT = By.xpath("//a[contains(text(),'Add New Client')]");
		
		public ClientTabPage(WebDriver driver)
		{
			this.driver = driver;
		}
		
		
		
				
				//Click on Add New Client
				
				public boolean clickOnAddNewClient()
				{
					try
					{
						waitForElement(driver, ADD_NEW_CLIENT, 30);
						driver.findElement(ADD_NEW_CLIENT).click();
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}

}
