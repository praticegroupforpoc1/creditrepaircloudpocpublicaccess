package com.corp.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.corp.test.utils.BaseTest;

public class LogonPage extends BaseTest
{
	
		private static final By USERNAME = By.xpath("//input[@id='username']");
		private static final By PASSWORD = By.xpath("//input[@id='password']");
		private static final By LOG_IN   = By.xpath("//input[@type='button']");
		
		public LogonPage(WebDriver driver)
		{
			this.driver = driver;
		}
		
		//Enter Username
		
		public boolean enterUsername(String userName)
		{
			try
			{
				waitForElement(driver, USERNAME, 30);
				driver.findElement(USERNAME).sendKeys(userName);
				
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
			}
			
			return true;
			
		}
		
		//Enter Password
		
				public boolean enterPassword(String password)
				{
					try
					{
						waitForElement(driver, PASSWORD, 30);
						driver.findElement(PASSWORD).sendKeys(password);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
				//Click Enter
				
				public boolean clickLogOn()
				{
					try
					{
						waitForElement(driver, LOG_IN, 30);
						driver.findElement(LOG_IN).click();
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}

}
