package com.corp.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.corp.test.utils.BaseTest;

public class AddNewClientPage extends BaseTest
{
	
	private static final By FIRST_NAME = By.xpath("//input[@id='fname']");
	private static final By LAST_NAME = By.xpath("//input[@id='lname']");
	private static final By EMAIL = By.xpath("//input[@id='email']");
	private static final By SSN = By.xpath("//input[@id='ssnumber']");
	private static final By PHONE_MOBILE = By.xpath("//input[@id='phonem']");
	private static final By MAILING_ADDRESS = By.xpath("//input[@id='addr']");
	private static final By CITY = By.xpath("//input[@id='city']");
	private static final By ZIP_CODE = By.xpath("//input[@id='pcode']");
	private static final By STATE = By.xpath("//select[@id='state_cmb']");
	private static final By SUBMIT = By.xpath("//input[@id='sub_button']");
	
		
		public AddNewClientPage(WebDriver driver)
		{
			this.driver = driver;
		}
		
		//Enter First Name
		
		public boolean enterFirstName(String fname)
		{
			try
			{
				waitForElement(driver, FIRST_NAME, 30);
				driver.findElement(FIRST_NAME).sendKeys(fname);
				
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
			}
			
			return true;
			
		}
		
		//Enter Last Name
		
				public boolean enterLastName(String lname)
				{
					try
					{
						waitForElement(driver, LAST_NAME, 30);
						driver.findElement(LAST_NAME).sendKeys(lname);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
				//Enter Email
				
				public boolean enterEmail(String mail)
				{
					try
					{
						waitForElement(driver, EMAIL, 30);
						driver.findElement(EMAIL).sendKeys(mail);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
                //Enter SSN
				
				public boolean enterLastFourDigitsOfSSN(String ssn)
				{
					try
					{
						waitForElement(driver, SSN, 30);
						driver.findElement(SSN).sendKeys(ssn);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
                    //Enter Phone Number
				
				public boolean enterPhoneNumber(String number)
				{
					try
					{
						waitForElement(driver, PHONE_MOBILE, 30);
						driver.findElement(PHONE_MOBILE).sendKeys(number);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
                  //Enter Mailing Address
				
				public boolean enterMailingAddress(String address)
				{
					try
					{
						waitForElement(driver, MAILING_ADDRESS, 30);
						driver.findElement(MAILING_ADDRESS).sendKeys(address);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
                        //Enter City
				
				public boolean enterCity(String city)
				{
					try
					{
						waitForElement(driver, CITY, 30);
						driver.findElement(CITY).sendKeys(city);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				
                   //Enter zipcode
				
				public boolean enterZipCode(String zcode)
				{
					try
					{
						waitForElement(driver, ZIP_CODE, 30);
						driver.findElement(ZIP_CODE).sendKeys(zcode);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
				

                //Select State
				
				public boolean selectState(String stateValue)
				{
					try
					{
						waitForElement(driver, STATE, 30);
						Select state = new Select(driver.findElement(STATE));
						state.selectByValue(stateValue);
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}
		
		
				
				//Click Submit button
				
				public boolean clickSubmit()
				{
					try
					{
						waitForElement(driver, SUBMIT, 30);
						driver.findElement(SUBMIT).click();
						
					}catch(NoSuchElementException e)
					{
						e.printStackTrace();
					}
					
					return true;
					
				}

}
